
function make2DArray(cols, rows) {
	var arr = new Array(cols);
	for (var i = 0; i < arr.length; i++) {
	arr[i] = new Array(rows);
	}
	return arr;
}

function roundUp(num, precision) {
  precision = Math.pow(10, precision)
  return Math.ceil(num * precision) / precision
}

function mathClamp(n, min, max){
	return Math.min(Math.max(min,n),max)
}

// Debug helpers
var pList = [];
var pVarList = new Array(3);

// Intensity factor (made up)
const intensityFactor = 0.5;
// const testChargeIntensity = 1.4;

// Coulomb's constant
const k = 8.9875517873681764*(10**9);

// deci
const d = 10**-1;
// centi
const c = 10**-2;
// mili
const m = 10**-3;
// micro (μ)
const mu = 10**-6;
// nano
const n = 10**-9;
// pico
const p = 10**-12;

var button;
var buttonPos;
var buttonColor;
var fontColor;
var fontSize;
var showIntensity = true;
var noIntensityColor;

var grid;
var cols;
var rows;
var w = 25;

var mouseClick = false;

var charges = [];
var chargesGrabbed = new Array();
var positiveChargeSpawn;
var negativeChargeSpawn;
var testChargeSpawn;
var chargeDefaultSize = 40;
var chargeNeeded = 3;  // It starts with a 3, meaning that all charges are needed.
var chargesToPush;

function setup() {

	createCanvas(801, 581)

	// for (var i = 0; i < pVarList.length; i++) {
	// 	pList.push(createP());
	// }

	cols = floor(width / w);
	rows = floor((height-80) / w);
	grid = make2DArray(cols, rows);
	for (var i = 0; i < cols; i++) {
		for (var j = 0; j < rows; j++) {
			grid[i][j] = new Cell(i, j, w);
		}
	}

	positiveChargeSpawn = {
						x: width*0.3,
						y: height-chargeDefaultSize}
	negativeChargeSpawn = {
						x: width*0.7,
						y: height-chargeDefaultSize}
	testChargeSpawn = {
					x: width*0.5,
					y: height-chargeDefaultSize}
	chargesToPush = [ 
					{x: testChargeSpawn.x, y: testChargeSpawn.y, w: chargeDefaultSize, charge: 0, active: false},
					{x: positiveChargeSpawn.x, y: positiveChargeSpawn.y, w: chargeDefaultSize, charge: 5*mu, active: false},
					{x: negativeChargeSpawn.x, y: negativeChargeSpawn.y, w: chargeDefaultSize, charge: -5*mu, active: false}]

	buttonColor = color(0, 0, 0, 0);
	fontColor = color(132, 132, 132, 255);
	fontSize = '24px';
	button = createButton(' Mostrar/Ocultar intensidad ');
	button.style('background-color', buttonColor);
	button.style('color', fontColor);
	button.style('font-size', fontSize);
	button.position(0, height);
	button.mouseClicked(showIntensityF);

	noIntensityColor = color('white');

	// var chargesParams = [[width*.25, height/2, 30, -20*mu], [width*.75, height/2, 30, 20*mu]];
	// for (var i = 0; i < chargesParams.length; i++) {
	// 	var p = chargesParams[i];
	// 	charges.push(new Charge(p[0], p[1], p[2], p[3]));
	// }
}

function draw() {

	background(0);

	push();
	noStroke();
	fill(125);
	rectMode(CENTER, CENTER);
	rect(width/2, height, width, 160);
	pop();


	if (chargeNeeded == 3) {
		for (var i = 0; i < 3; i++) {
			charges.push(new Charge(chargesToPush[i]));
		}
		chargeNeeded = -1;
	}

	if (chargeNeeded != -1) {
		charges.push(new Charge(chargesToPush[chargeNeeded]));
		chargeNeeded = -1;
	}

	// for (var i = 0; i < pList.length; i++) {
 //  		pList[i].html("Debug Var: " + pVarList[i]);
 //  	}

 	if (charges.every(elem => ((elem.getElectricFieldVector()).mag() == 0) == true)) {
		pVarList[0] = 0;
 	}

 	for (var i = 0; i < pVarList.length; i++) {	
		if (pVarList[i] && !pList[i]) {
			pList.push(createP());
		}
 	}

 	if (showIntensity) {
	  	for (var i = 0; i < pList.length; i++) {
	  		if (i == 0) {
	  			pList[i].html("<br>Intensidad del campo: " + pVarList[i] + "<br>(en la ultima carga de prueba creada)");
	  		} else {
	  			pList[i].html("Intensidad del campo: " + pVarList[i]);
	  		}
	  	}
	} else {
		try {
			pList[0].html("");
		} catch (err) {}
	}

  	for (var i = charges.length - 1; i >= 0; i--) {
		var c = charges[i];

		if (dist(mouseX, mouseY, c.x, c.y) <= c.w/2) {

			if (mouseClick && chargesGrabbed.every(elem => elem == false)) {
				chargesGrabbed[i] = true;
				charges[i].active = true;
			}
		}
	}

	if (charges.some(elem => (dist(mouseX, mouseY, elem.x, elem.y) <= elem.w/2) == true)) {
		cursor(HAND);
	} else {
		cursor(ARROW);
	}

	for (var i = 0; i < charges.length; i++) {
		if (chargesGrabbed[i]) {
			charges[i].x = mouseX;
			charges[i].y = mouseY;
			charges[i].x = mathClamp(charges[i].x, 0, width);
			charges[i].y = mathClamp(charges[i].y, 0, height);
		}
	}

	for (var i = 0; i < cols; i++) {
		for (var j = 0; j < rows; j++) {

			grid[i][j].update();
			if (showIntensity) {
				grid[i][j].show();
			} else {
				grid[i][j].show(noIntensityColor);
			}
		}
	}

	for (var i = 0; i < charges.length; i++) {
		charges[i].show();
	}

}

function mousePressed() {
	mouseClick = true;
}

function mouseClicked() {
	mouseClick = false;
	for (var i = 0; i < chargesGrabbed.length; i++) {
		if (chargesGrabbed[i] == true) {
			chargesGrabbed[i] = false;
			// THIS COULD EASILY BE A 'FOR' LOOP.
			if (charges[i].charge == 0) {
				if (charges[i].y + charges[i].w/2 > testChargeSpawn.y - chargeDefaultSize) {
					if (charges[i].active){
						try {
							charges.splice(i, 1);
							pVarList[0] = 0;
							chargeNeeded = 0;
						} catch(err) {chargeNeeded = 0}
					}
				}
				else {
					if (charges[i].active){
						chargeNeeded = 0;
					}
				}
			}
			else if (charges[i].charge > 0){
				if (charges[i].y + charges[i].w/2 > positiveChargeSpawn.y - chargeDefaultSize) {
					if (charges[i].active){
						try {
							charges.splice(i, 1);
							chargeNeeded = 1;
						} catch(err) {chargeNeeded = 1}
					}
				}
				else {
					if (charges[i].active){
						chargeNeeded = 1;
					}
				}
			}
			else {
				if (charges[i].y + charges[i].w/2 > negativeChargeSpawn.y - chargeDefaultSize) {
					if (charges[i].active){
						try {
							charges.splice(i, 1);
							chargeNeeded = 2;
						} catch(err) {chargeNeeded = 2}
					}
				}
				else {
					if (charges[i].active){
						chargeNeeded = 2;
					}
				}
			}
		}
	}
}

function showIntensityF() {
	showIntensity = !showIntensity;
}
