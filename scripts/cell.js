function Cell(i, j, w) {
	this.i = i;
	this.j = j;
	this.x = i*w + w/2;
	this.y = j*w + w/2;
	this.w = w;
	// this.color = color('rgba(171, 52, 52, 255)');
	this.color = color("white");
	this.voltage;
	this.electricFieldCurrentVector;
}

Cell.prototype.show = function(defaultColor=null) {
	// stroke(0);
	// fill(200);
	// rect(this.x, this.y, this.w, this.w);
	push();
	translate(this.x, this.y);
	// angleMode(RADIANS);
	// rotate(PI / this.angle);
	rotate(this.electricFieldCurrentVector.heading());
	noStroke();
	if (defaultColor) {
		if (charges.some(elem => (elem.charge != 0) && ((elem.active) == true))) {  // If there's at least one active charge.
			fill(defaultColor);
		} else {
			noFill();
		}
	} else {
		fill(this.color);
	}
	triangle(-this.w/3, this.w/4, -this.w/3, -this.w/4, this.w/3, 0);
	pop();
}

Cell.prototype.update = function() {
	this.updateRotation();
	this.updateColor();
}

Cell.prototype.updateRotation = function() {
	this.electricFieldCurrentVector = this.getElectricFieldVector();
}

Cell.prototype.updateColor = function() {
	var from = color(0, 52, 115, 0);
	var to = color(216, 40, 43, 255);
	this.voltage = (this.getVoltage());
	// this.color = lerpColor(from, to, abs(this.electricFieldCurrentVector.mag())*intensityFactor);
	this.color.setAlpha(this.electricFieldCurrentVector.mag()*intensityFactor);

}

Cell.prototype.getElectricField = function() {
	EFSum = 0;
	for (var i = 0; i < charges.length; i++) {
		var c = charges[i];
		if (c.active) {
			EFSum += k * ( c.charge / (dist(c.x, c.y, this.x, this.y)**2) );
		}
	}
	return EFSum;
}

Cell.prototype.getElectricFieldVector = function() {
	EFVSum = createVector(0, 0);
	for (var i = 0; i < charges.length; i++) {
		var c = charges[i];
		if (c.active) {
			var vec = createVector(this.x - c.x, this.y - c.y);
			EFVSum.add(vec.mult(k*(c.charge/(dist(c.x, c.y, this.x, this.y)**2))));
		}
	}
	return EFVSum;
}

Cell.prototype.getVoltage = function() {
	var voltage_ = 0;
	for (var i = 0; i < charges.length; i++) {
		var c = charges[i];
		if (c.active) {
			voltage_ += k * c.charge / dist(c.x, c.y, this.x, this.y);
		}
	}
	return voltage_;
}
