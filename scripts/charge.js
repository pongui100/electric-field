function Charge(params) {  // x, y, w, charge, active=false
	this.x = params.x;
	this.y = params.y;
	this.w = params.w;
	this.charge = params.charge;
	this.active = params.active;

	// Electric field vector from the current position.
	this.EFVector = createVector(0, 0);
}

Charge.prototype.show = function() {

	push();
	noStroke();
	var signColor;
	if (this.charge < 0) {
		fill(47,93,219);
		signColor = [47,93,219];
	}
	else if (this.charge > 0) {
		fill(235,38,41);
		signColor = [235,38,41];
	}
	else {
		fill(132,183,192);
		signColor = [132,183,192];
	}
	ellipse(this.x, this.y, this.w, this.w);
	strokeWeight(2);
	fill(0);
	textAlign(CENTER, CENTER);
	textSize(this.w*.8);
	var sign;
	if (this.charge < 0) {
		sign = "-";
	}
	else if (this.charge > 0) {
		sign = "+";
	}
	else {
		sign = "0";
	}
	//text(sign, this.x, this.y + this.w/3);
	fill(255);
	text(sign, this.x, this.y);

	textAlign(CENTER, TOP);
	textSize(10);
	fill(signColor[0] + 20, signColor[1] + 20, signColor[2] + 20);
	text(roundUp(this.charge, 6) + "[C]", this.x, this.y + this.w/2);
	pop();

	this.EFVector = this.getElectricFieldVector();
	if (this.charge == 0 && this.active && !(this.EFVector.x == 0 && this.EFVector.y == 0)) {
		push();
		strokeWeight(6);
		stroke(233,208,98);
		translate(this.x, this.y);
		line(0, 0, this.EFVector.x, this.EFVector.y);
		translate(this.EFVector.x, this.EFVector.y);
		ellipse(0, 0, 8, 8);
		pop();

		pVarList[0] = this.EFVector.mag() + " [N/C]";
	}
	
}

Charge.prototype.getElectricFieldVector = function() {
	EFVSum = createVector(0, 0);
	for (var i = 0; i < charges.length; i++) {
		var c = charges[i];
		if (c.active) {
			var vec = createVector(this.x - c.x, this.y - c.y);
			EFVSum.add(vec.mult(k*(c.charge/(dist(c.x, c.y, this.x, this.y)**2))));
		}
	}
	return EFVSum;
}
